﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using telegrambot.Database.Models;

namespace telegrambot.Database.Handlers
{
    class DatabaseContext : DbContext

    {
        private string server = "localhost;";
        private string userId = "root";
        private string password = "password";
        private string database = "telebot";
        public DbSet<User> Users { get; set; }

        public DatabaseContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //base.OnConfiguring(optionsBuilder);
            optionsBuilder
                .UseMySql(
                "server=" + server + ";" +
                "UserId=" + userId + ";" +
                "Password=" + password + ";" +
                "database=" + database + ";");
        }
    }
}
