﻿using System;
using System.Collections.Generic;
using System.Text;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace telegrambot
{
    class Bot
    {
        private TelegramBotClient client;
        public Bot(string token)
        {
            client = new TelegramBotClient(token);
            client.OnMessage += OnMessageReceived;
            client.OnMessageEdited += OnMessageReceived;
            client.StartReceiving();
            Console.ReadLine();
            client.StopReceiving();
        }


  

        private async void OnMessageReceived(object sender, MessageEventArgs e)
        {
         
            var message = e.Message;
            if (message?.Type == Telegram.Bot.Types.Enums.MessageType.Text)
            {
                switch (message.Text)
                {
                    case "/start":
                        await client.SendTextMessageAsync(message.Chat.Id, "bot activated", ParseMode.Default, false, false, 0, GetKeyboard());
                        break;
                    case "GENERATE":
                        string pass = Generator.generatePassword();
                        await client.SendTextMessageAsync(message.Chat.Id, pass, ParseMode.Default, false, false, 0, GetKeyboard());
                        Console.WriteLine(message.Chat.LastName + ": " + pass);
                        break;
                }
            }
        }


        private ReplyKeyboardMarkup GetKeyboard() {
            var rkm = new ReplyKeyboardMarkup();
            rkm.Keyboard =
                new KeyboardButton[][]
                {
                    new KeyboardButton[]
                    {
                       new KeyboardButton("GENERATE"),
                    }
                };
            return rkm;
        }
    }
}
