﻿using System;
using System.Collections.Generic;
using System.Text;

namespace telegrambot.Database.Models
{
    class User
    {
        public int Id { get; set; }
        public int Number { get; set; }
        public string Password { get; set; }
    }
}
